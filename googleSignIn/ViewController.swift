//
//  ViewController.swift
//  googleSignIn
//
//  Created by Apple on 23/02/16.
//  Copyright © 2016 jonty. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, GIDSignInUIDelegate {

    @IBOutlet weak var signinButton: GIDSignInButton!
    
    @IBOutlet weak var myActivityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor.lightGrayColor()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        GIDSignIn.sharedInstance().uiDelegate = self
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "moveToList", name: "DataSaved", object: nil)
        // Do any additional setup after loading the view, typically from a nib.
    } 

    @IBAction func signinClick(sender: AnyObject) {
        myActivityIndicator!.startAnimating()
        
        GIDSignIn.sharedInstance().signIn()
    }
    
    // Stop the UIActivityIndicatorView animation that was started when the user
    // pressed the Sign In button
    func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
        myActivityIndicator!.stopAnimating()
    }
    
    //MARK:- Show List Controller
    func moveToList() {
        myActivityIndicator!.stopAnimating()
        self.performSegueWithIdentifier("ListController", sender: self)
    }
    
    // Present a view that prompts the user to sign in with Google
    func signIn(signIn: GIDSignIn!,
        presentViewController viewController: UIViewController!) {
            self.presentViewController(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func signIn(signIn: GIDSignIn!,
        dismissViewController viewController: UIViewController!) {
            self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

