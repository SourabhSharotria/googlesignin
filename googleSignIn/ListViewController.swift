//
//  ListViewController.swift
//  googleSignIn
//
//  Created by Apple on 24/02/16.
//  Copyright © 2016 jonty. All rights reserved.
//

import UIKit
import CoreData

class ListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var usernameArray = [String]()
    @IBOutlet weak var usernameTableView: UITableView!
    
    let appDelegate =
    UIApplication.sharedApplication().delegate as! AppDelegate
    var userDetail = [NSManagedObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Users"

        self.navigationController?.navigationBar.barTintColor = UIColor.lightGrayColor()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        self.fetchData()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(animated: Bool) {
        GIDSignIn.sharedInstance().signOut()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - TableView Delegates
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return usernameArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell  {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! UserListTableViewCell
        cell.usernameLabel.textAlignment = .Left
        
        cell.usernameLabel.text = usernameArray[indexPath.row]
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    
    //MARK:- Fecth Messages
    
    func fetchData () {
        let fetchRequest = NSFetchRequest(entityName: "UserDetail")
        
        let sortDescriptor = NSSortDescriptor(key: "time", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        //3
        do {
            let results =
            try appDelegate.managedObjectContext.executeFetchRequest(fetchRequest)
            userDetail = results as! [NSManagedObject]
            
            usernameArray.removeAll()
            for item in userDetail {
                
                usernameArray.append((item as! UserDetail).username!)
            }
            
            usernameTableView.reloadData()
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

class UserListTableViewCell : UITableViewCell {
    @IBOutlet weak var usernameLabel: UILabel!
}
