//
//  UserDetail+CoreDataProperties.swift
//  
//
//  Created by Apple on 24/02/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension UserDetail {

    @NSManaged var username: String?
    @NSManaged var email: String?
    @NSManaged var userid: String?
    @NSManaged var time: NSDate?
    
}
